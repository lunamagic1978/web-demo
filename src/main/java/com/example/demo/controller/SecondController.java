package com.example.demo.controller;

import com.example.demo.service.SecondService;
import com.example.demo.service.ThirdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecondController {

    @Autowired
    private SecondService secondService;

    @Autowired
    private ThirdService thirdService;

    @GetMapping("/second")
    // 对应接口
    public String second(@RequestParam(value="username") String userName){
        // 如果用户名是admin，调用secondService的secondDemo
        if (userName.equals("admin")){
         SecondService.secondDemo();
         return "username is admin";
        }
        else
        // 如果用户名不是admin，调用的是thirdService的thirdDemo
        {
            ThirdService.thirdDemo();
            return "username is" + userName;
        }
    }
}
