package com.example.demo.controller;

import com.example.demo.service.FirstService;
import com.example.demo.service.SecondService;
import com.example.demo.service.ThirdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ThirdController {

    @Autowired
    private FirstService firstService;

    @Autowired
    private SecondService secondService;

    @Autowired
    private ThirdService thirdService;

    @GetMapping("/third")
    // 对应接口
    public String third(@RequestParam(value="username") String userName){
        System.out.println(userName);
        // 如果用户名是admin，调用thirdService的thirdDemo
        if (userName.equals("admin")){
            ThirdService.thirdDemo();
            return "in firstDemo";
        }
        // 如果用户名不是admin，调用的是secondService的secondDemo
        else{
            SecondService.secondDemo();
            return "in secondDemo";
        }
    }
}
