package com.example.demo.controller;
import com.example.demo.service.FourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FourController {

    @Autowired
    private FourService fourService;


    @GetMapping("/four")
    public String four(){
        fourService.fourDemo();
        return "four";
    }
}
