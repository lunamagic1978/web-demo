package com.example.demo.controller;

import com.example.demo.service.FirstService;
import com.example.demo.service.SecondService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FirstController {

    @Autowired
    private FirstService firstService;

    @Autowired
    private SecondService secondService;

    @GetMapping("/first")
    public String first(@RequestParam(value="username") String userName){
        System.out.println(userName);
        if (userName.equals("admin")){
            firstService.firstDemo();
            return "in firstDemo";
        }else{
            secondService.secondDemo();
            return "in secondDemo";
        }
    }
}
